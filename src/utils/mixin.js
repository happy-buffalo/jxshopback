import Vue from 'vue'

// 跳转
Vue.mixin({
    methods: {
        // 跳转 
        jump (url) {
            this.$router.push({
                path: url
            })
        }
    }
})