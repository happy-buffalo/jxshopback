import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'


// 配置 ElementUI
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
Vue.use(ElementUI);


// 配置 全局初始化样式
import '@/assets/css/reset.scss';


//配置跳转混入
import '@/utils/mixin.js'

//配置excel
import JsonExcel from 'vue-json-excel'
Vue.component('downloadExcel', JsonExcel)

//配置粒子特效
import VueParticles from 'vue-particles'
Vue.use(VueParticles)

//配置animate.css
import animated from 'animate.css'
Vue.use(animated)

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
